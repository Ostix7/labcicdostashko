package com.company;

import com.company.annotation_processor.CustomProcessor;
import com.company.anotations.Alarm;
import com.company.anotations.Hobby;

import java.lang.annotation.Annotation;
import java.util.Objects;

public class Main {

    public static void main(String[] args) {


        Person zhenya = new com.company.impl.Person();
        checkIfAlarm(zhenya);


    }
    private static void checkIfAlarm(Object object) {
        if (Objects.isNull(object)) {
            throw new RuntimeException("The object  is null");
        }

        Class<?> clazz = object.getClass();
        if (!clazz.isAnnotationPresent((Alarm.class))) {
            throw new RuntimeException("The class "
                    + clazz.getSimpleName()
                    + " is not annotated with Alartm");
        }
        else{
            System.out.println("class has alarm anotation");
        }
    }
}
