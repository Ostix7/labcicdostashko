package com.company.impl;

import com.company.anotations.BuilderProperty;
import com.company.anotations.Hobby;

@Hobby(name = "football")
public class Student extends Person{
    private String university;
    private String faculty;

    @BuilderProperty
    public void setAge(String faculty) {
        this.faculty = faculty;
    }

    @BuilderProperty
    public void setUniversity(String university) {
        this.university = university;
    }
}
