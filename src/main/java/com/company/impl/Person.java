package com.company.impl;

import com.company.anotations.Alarm;
import com.company.anotations.FieldAlarm;
import com.company.anotations.Marker;

@Alarm
public class Person implements com.company.Person {

    @Alarm
    public Person(){

        System.out.println("Person created");
    }

    String name;

    @FieldAlarm(key="Bob")
    String female;

    @Override
    public void learn() {
        System.out.println("Person is learning");
    }

    @Override
    public void eat() {
        System.out.println("Person is eating");
    }

    @Override
    public void sleep() {
        System.out.println("Person is sleap");
    }

}
