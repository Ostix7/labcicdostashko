package com.company;

public interface Person {
     void learn();
     void eat();
     void sleep();
}
