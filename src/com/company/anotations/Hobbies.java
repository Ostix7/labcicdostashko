package com.company.anotations;

public @interface Hobbies{
   Hobby[] value();
}