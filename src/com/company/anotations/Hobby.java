package com.company.anotations;

import java.lang.annotation.Repeatable;

@Repeatable(Hobbies.class)
public @interface Hobby {
    String name();
    int yearsSpent() default 1;

}
