package com.company.impl;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @org.junit.jupiter.api.Test
    void learn() {
        Person person = new Person();
        person.name="a";
        assertEquals(person.name,"a");
    }

    @org.junit.jupiter.api.Test
    void eat() {
        Person person = new Person();
        person.female="a";
        assertEquals(person.female,"a");
    }


}